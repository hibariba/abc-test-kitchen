kitchen
=======

## Bare minimum to run the _test kitchen_ job

### dependencies
- [ruby](./docs/installing_rbenv.md)
- [test kitchen](http://kitchen.ci/)

    ```bash
    gem install test-kitchen
    ```

### FILES:

#### config

general config
[.kitchen.yml](.kitchen.yml)
```yaml
---
driver:
  name: vagrant

provisioner:
  name: chef_solo

platforms:
  - name: ubuntu-12.04

suites:
  - name: default
    run_list:
      - recipe[git::default]
    attributes:

```

cookbook namespace
[metadata.rb](./metadata.rb)
```ruby
name "git"
version "0.1.0"

```


#### recipes

[recipes/default.rb](./recipes/default.rb)
```ruby
package "git"

```

#### tests

empty folder - placeholder for tests
[test/integration/default/]( test/integration/default/)
